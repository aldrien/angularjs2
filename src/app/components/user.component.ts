import { Component } from '@angular/core';
import { PostsService } from '../services/post.service';


@Component({
  moduleId: module.id,
  selector: 'user',
  templateUrl: 'user.component.html',
  providers: [PostsService]
})
export class UserComponent  { 
	name: string;
	email: string;
	address: address;
	hobbies: string[];
	showHobbies: boolean;
	posts: Post[];

	// runs every time component is rendered
	constructor(private postsService: PostsService){
		console.log('constructor runs...')

		this.name = 'Aldrien Hate';
		this.email = 'aldrien@yahoo.com';
		this.address = {
			street: 'Maryland St. San Pablo',
			city: 'Dinalupihan',
			state: 'Bataan'
		}
		this.hobbies = ['Music', 'Movies', 'Sports'];
		this.showHobbies = false;

		this.postsService.getPosts().subscribe(posts => {
			this.posts = posts;
		});
	}

	// Show & Hide Hobbies
	toggleHobbies(){
		if(this.showHobbies == true){
			this.showHobbies = false;
		}else{
			this.showHobbies = true;
		}
		
	}

	// Add Hobby
	addHobby(hobby){
		this.hobbies.push(hobby);
	}

	// Delete Hobby
	deleteHobby(index){
		this.hobbies.splice(index, 1)
	}

	// Delete Post
	deletePost(id){
		this.postsService.deletePost(id);
	}
}

interface address {
	street: string;
	city: string;
	state: string;
}

interface Post {
	id: number;
	title: string;
	body: string;
}
import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  	<nav>
  		<ul>
  			<li><a routerLink="/">Home</a></li>
  			<li><a routerLink="/about">About</a></li>
  		</ul>
  	</nav>
  	<hr>
  	<router-outlet></router-outlet>
  `,
})
export class AppComponent  {
}
